package com.example.attest3.controller;

import com.example.attest3.models.Product;
import com.example.attest3.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;

@Controller
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/product")

    public String addProduct(@RequestParam("id") Integer id,
                             @RequestParam("description") String description,
                             @RequestParam("cost") Integer cost,
                             @RequestParam("quantity") Integer quantity){

       Product product = Product.builder()
               .Id(id)
                .description(description)
                .cost(cost)
                .quantity(quantity)
                .build();

        productRepository.save(product);

        return "redirect:/ProductsAdd.html ";
    }

}
