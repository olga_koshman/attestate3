package com.example.attest3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attest3Application {

    public static void main(String[] args) {
        SpringApplication.run(Attest3Application.class, args);
    }

}
