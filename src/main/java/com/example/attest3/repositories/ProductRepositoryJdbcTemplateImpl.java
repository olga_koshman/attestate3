package com.example.attest3.repositories;

import com.example.attest3.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
@Component
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    private static final String SQL_INSERT = "insert into product (id, description, cost, quantity) values (?, ?, ?, ?)";
    private static final String SQL_SELECT_ALL = "Select * from product order by id";
    private static final String SQL_SELECT_ALL_BY_COST = "Select * from product where cost =";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public static RowMapper<Product> productRowMapper = (row, rowNum) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        int cost = row.getInt("cost");
        int quantity = row.getInt("quantity");

        return new Product(id, description, cost, quantity);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getId(), product.getDescription(), product.getCost(), product.getQuantity());
    }

    @Override
    public List<Product> findAllByPrice(int cost) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_COST + cost, productRowMapper);
    }
}
