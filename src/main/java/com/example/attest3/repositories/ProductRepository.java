package com.example.attest3.repositories;

import com.example.attest3.models.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();
    public void save(Product product);
    List<Product> findAllByPrice(int price);

}
